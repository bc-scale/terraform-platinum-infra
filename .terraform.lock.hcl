# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.4.0"
  constraints = "~> 3.4.0"
  hashes = [
    "h1:m8e6PglbTmW+uSRjlJpIlCieYPO5D4AMYq/G/WjlkW0=",
    "zh:10a8d8fe534dea7d0acf7ee1809ae71cc7fa2dbf2db14ac5816b16a6798a13f0",
    "zh:1d8fbbb65b308c2de4a3c09dfd6edd47f2ac5c53d13f48af5d6a422cd44d8711",
    "zh:273350cfae04322b76d28d8fff8adff8dc307dd945f88c60239f37df6b447022",
    "zh:2a69ec99546c1d8b2a060c7034209e94cb7e355cef71d1d00433de5359860de0",
    "zh:452c23d867de1a1a564c9972765b6570867af6c87dcee303257dc06c1c765fd3",
    "zh:66c17bca27a2e63e63a91db28f50117541d236979e8f45251921bc1f17dcff68",
    "zh:683e5891d6f79b202b456e2b8d5a4d2906949de6028cddbaa46c40a193197089",
    "zh:843ae2f88f726df6375b64fc883861b4ca7d4b4488ab7881409fe66cf42c6956",
    "zh:8687e787de8fde78d4ea24487ed229ef1e33035a52a9d3f7915ceafdd027dc8f",
    "zh:a0c543589d3c8b94c4409c8bea1934f671fc21df18251bf78eac0e69b8d9161c",
    "zh:a4a2310b308c2312b08839cec30846666a6fa5032758fe77c03ef485738d931d",
    "zh:b27e56e0fadf5b2b970c6d9b642eafd4fddbf8ed2d5ba730895a94a6e8fcebbd",
    "zh:bf1fb56214cc9c174e2919e2c08015615b72d2fedc7b991c165afdc50163c794",
    "zh:fd37eb9609a949269ffa66ee830bf0758de31fb4be0d500c33b6397acb108c5c",
  ]
}

provider "registry.terraform.io/hashicorp/nomad" {
  version     = "1.4.16"
  constraints = "~> 1.4.16"
  hashes = [
    "h1:PQxNPNmMVOErxryTWIJwr22k95DTSODmgRylqjc2TjI=",
    "zh:0d4fbb7030d9caac3b123e60afa44f50c83cc2a983e1866aec7f30414abe7b0e",
    "zh:0db080228e07c72d6d8ca8c45249d6f97cd0189fce82a77abbdcd49a52e57572",
    "zh:0df88393271078533a217654b96f0672c60eb59570d72e6aefcb839eea87a7a0",
    "zh:2883b335bb6044b0db6a00e602d6926c047c7f330294a73a90d089f98b24d084",
    "zh:390158d928009a041b3a182bdd82376b50530805ae92be2b84ed7c3b0fa902a0",
    "zh:7169b8f8df4b8e9659c49043848fd5f7f8473d0471f67815e8b04980f827f5ef",
    "zh:9417ee1383b1edd137024882d7035be4dca51fb4f725ca00ed87729086ec1755",
    "zh:a22910b5a29eeab5610350700b4899267c1b09b66cf21f7e4d06afc61d425800",
    "zh:a6185c9cd7aa458cd81861058ba568b6411fbac344373a20155e20256f4a7557",
    "zh:b6260ca9f034df1b47905b4e2a9c33b67dbf77224a694d5b10fb09ae92ffad4c",
    "zh:d87c12a6a7768f2b6c2a59495c7dc00f9ecc52b1b868331d4c284f791e278a1e",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "3.1.0"
  hashes = [
    "h1:fUJX8Zxx38e2kBln+zWr1Tl41X+OuiE++REjrEyiOM4=",
    "zh:3d46616b41fea215566f4a957b6d3a1aa43f1f75c26776d72a98bdba79439db6",
    "zh:623a203817a6dafa86f1b4141b645159e07ec418c82fe40acd4d2a27543cbaa2",
    "zh:668217e78b210a6572e7b0ecb4134a6781cc4d738f4f5d09eb756085b082592e",
    "zh:95354df03710691773c8f50a32e31fca25f124b7f3d6078265fdf3c4e1384dca",
    "zh:9f97ab190380430d57392303e3f36f4f7835c74ea83276baa98d6b9a997c3698",
    "zh:a16f0bab665f8d933e95ca055b9c8d5707f1a0dd8c8ecca6c13091f40dc1e99d",
    "zh:be274d5008c24dc0d6540c19e22dbb31ee6bfdd0b2cddd4d97f3cd8a8d657841",
    "zh:d5faa9dce0a5fc9d26b2463cea5be35f8586ab75030e7fa4d4920cd73ee26989",
    "zh:e9b672210b7fb410780e7b429975adcc76dd557738ecc7c890ea18942eb321a5",
    "zh:eb1f8368573d2370605d6dbf60f9aaa5b64e55741d96b5fb026dbfe91de67c0d",
    "zh:fc1e12b713837b85daf6c3bb703d7795eaf1c5177aebae1afcf811dd7009f4b0",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.32.1"
  constraints = "~> 1.32.1"
  hashes = [
    "h1:RfLu8m+y3fKf5FrDJarSp06KS4R75yZxPy9n+Df5PjM=",
    "zh:043e941caf46b3a37cae5f2f9c1b7ce2b30f0b492bada6f3d8d6a7384f5cb7b2",
    "zh:055835d483dd172e7b0e500f9dd789353e32c9328d51793b8d88451df4e92067",
    "zh:3dd5a0006ab7f464a2bca8c5a46e583c6f09ba66aeff1ca847397b61fc823597",
    "zh:3f0444956fdcb059ee9ea54f51af016d86f297477335f519256ca158a75c5e59",
    "zh:569a80f0c9e2f5fb121d9050bc10d6e6ba30507e2e985b809a2613dcb5bdc095",
    "zh:5e7e8499e62408d784d4c886827d421962134a3efda5f5f4f8794f9b1c17190c",
    "zh:67b48380e144ba4c31fff41442cbf53463eba285321d4283430b605285048923",
    "zh:7ddc434dbefecc6b1934f683f54ad5552c9e466b5e256b9cfe67f7b28ffecc7d",
    "zh:87c0b5f4f6b3121cc81935ccb8598a58bda20c7f96f8a4270ecb0b6b2096ba40",
    "zh:891d2234146c3fbc2fe6d2a0c176cefd01d16d2d1d25eebe6e15909aac4a1ddf",
    "zh:a90ced7f84d8bdd64afd00c69ea9e2b1ed43314da020860c31ff266b3716d1f0",
    "zh:b8c86266d9f4ae4d2cca8f3f7d58a48d0c000f16aa21a733bb81c760efa690f7",
    "zh:bf8fdd6eb8619dc20d85d418ed910a79af0b28bf79ac3a4029f0d0ae032c9c7d",
    "zh:d6d87b405c0fd5e576a7e0a8976689d555299806484fdacca205537367d92f37",
  ]
}

provider "registry.terraform.io/toowoxx/packer" {
  version     = "0.8.1"
  constraints = "0.8.1"
  hashes = [
    "h1:ghK4dViSDU8Dxj0sUqIHgtxD5o2RG6MBXEagozTRhkI=",
    "zh:00c676511bb999d6eb8dcd520cec586483ded287fa68ea2929ec114e40bfd7fb",
    "zh:0fdb381653a71abae325e52f60e3666f167671c52cbf9b1404551a4f27cc7db6",
    "zh:48c8647c488662c2f26abcdc84499c4d8091d37abe2420f05fa09fa9ba7a9fb8",
    "zh:5bdc40c50ca0e15cebff62ca45000a3dfc569304d514a00c88fce12c3e86d95b",
    "zh:63251b63a2f3ae8f0848e1dccb6f329301badfdbc4c9f3be3064366698c47005",
    "zh:83b9aae455bd7e0455518b840448e884e53d011425c78e5372d6752c4e7a44bb",
    "zh:cb91aad848522c677297bd2bfb1da18c378df96f73cf21fd1faa8d0ba23ee0fb",
    "zh:df1a4f9ad189600ecb0c0bac833d70708f30d278e4cf6e3358e5919cf48d29d4",
    "zh:fa0161280498de2ccf4a40b9727ae99a90a5793e8532f9d7c2bed594fc71a060",
  ]
}

provider "registry.terraform.io/xdevs23/network" {
  version     = "0.0.1-beta.2"
  constraints = "0.0.1-beta.2"
  hashes = [
    "h1:jVv9eScWe4YrW11ecERsNmkBLJdeeyNDZxe+B4dIdo8=",
    "zh:18ef5a1d1b809ce8e219a523f23300def159a74a5b2ace7f24f3fb51f4e05069",
    "zh:1f1ba1d59fe9266e504d2fb6244b5ccab412bedb6a88a0ab9088a1062735b21e",
    "zh:3e66e0d418cd03d1ea84ff4dd7818d57f1c18ddbeaac5c81215e6def4194c113",
    "zh:6f5724c03094d1301744af3686d67c4ff2f87627f5835431a240147d1ff9aaa9",
    "zh:9e5bb1a3a876e5892e4f59195a988a056f224c231bed1392ac25557cf91c9072",
    "zh:ba55eed1536bfa4a3cc222069f7f6fc20f457de2156d9a0873bbbdc303b2a95c",
    "zh:bfc276a848afc661497f6bad8f6389638ca7cff298d2a766a5f8a3cc16f5523f",
    "zh:e0223a8b1e82fcc599ff29d7e35c8c2dd7f30bce04d8dcda21e0d36bcd3dcfcb",
    "zh:e13ee234ed7add4c4b63a9c8a0636420e0ec248cc8c7713f3c88cf08b8f2f206",
    "zh:e9b49b79826628824821d0d529984af9823d0821c9f653781f2122df41c0bec5",
  ]
}
